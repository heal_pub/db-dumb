# HEAL2023 database installation manual

This manual suggests how to upload HEAL2023 solution database's dump onto the PostgreSQL Docker container and configurate it.

# Install Docker PostgreSQL database

## Pre-requirements

* Your Docker environment must have a data volume named as "pq_heal_data", if not, create it by the command:

> docker volume create pq_heal_data

* Your Docker environment mush have a network named ad "heal_net", if not, create it by the command:

> docker network create heal_net

## Install PostgreSQL dababase

Run the following command:
> docker run --name postgres -e POSTGRES_PASSWORD=HEAL2023 --network heal_net -v pq_heal_data:/var/lib/postgresql/data -d postgres

## Restore database dump

Create your working directory and move into. For, example:

> cd dump_woking_dir

Clone the repository:

> git clone https://gitlab.com/heal_pub/db_dump.git

> cd db_dump

Use the latest dump file available in the current repository. For example, "12-07-23.tar".

Prepare a docker container to manage dump files:

> docker run -v pq_heal_data:/dbdata --name dbrestore ubuntu /bin/bash

Upload the dump file into the Docker container named "postgres". Replace [**latest_file.tar**] with the real dump file name. By the following command:

*in linux*:

> docker run --rm --volumes-from dbrestore -v $(pwd):/backup ubuntu bash -c "cd /dbdata && tar xvf /backup/[**latest_file.tar**] --strip 1"

*in Windows PowerShell*:

> docker run --rm --volumes-from dbrestore -v ${pwd}:/backup ubuntu bash -c "cd /dbdata && tar xvf /backup/[**latest_file.tar**] --strip 1"

Done.
